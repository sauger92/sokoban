/************************************************************
Sokoban project - Maze file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/

#include "maze.h"
#include "graphic.h"
#include "noeud.h"
#include "Sommet.h"

#include <cmath>
#include <stack>
#include <queue>
#include "utils/console.h"
#include "utils/coord.h"
#include <fstream>
#include <iomanip>
#include "time.h"
#include <unistd.h>
#include <chrono>
#include <thread>

Maze::Maze(const std::string& path)
    : m_lig(0), m_col(0), m_pos_player(0), m_dir_player(TOP), m_level_path(path), m_deadlock(0)
{
}

Maze::~Maze()
{
}

bool Maze::init()
{
    bool res = this->_load(this->m_level_path);
    if (!res)
    {
        std::cerr << "Cannot load maze... Check file : " << this->m_level_path << std::endl;
        return false;
    }

    return res;
}

// Check if all boxes are on a goal
bool Maze::_isCompleted() const
{
    for (unsigned int i=0; i<this->m_pos_boxes.size(); ++i)
    {
        if (!this->isSquareBoxPlaced(this->m_pos_boxes[i]))
            return false;
    }
    return true;
}

// Check if we can push a box in a direction
// INPUT: position of the box to check, direction,
// OUTPUT : the position of the box after pushing
//      TRUE if all goes right otherwise FALSE
bool Maze::_canPushBox(unsigned short posBox, char dir, unsigned short& newPosBox) const
{
    // Check if this position is a box !
    if (!this->isSquareBox(posBox))
        return false;

    // Compute new position according to push direction
    newPosBox = Coord::getDirPos(posBox, dir);

    // Can we push the box ?
    return this->isSquareWalkable(newPosBox);
}

// Load a maze from a file (DO NOT TOUCH)
bool Maze::_load(const std::string& path)
{
    std::vector<unsigned short> tmpPosBoxes;
    std::ifstream ifs(path.c_str());
    if (ifs)
    {
        std::vector<std::string> lines;
        std::string line;
        while (std::getline(ifs, line))
        {
            lines.push_back(line);
            this->m_lig++;
            this->m_col = (this->m_col < line.size() ? line.size() : this->m_col);
        }
        ifs.close();

        if (this->m_col > NB_MAX_WIDTH || this->m_lig > NB_MAX_HEIGHT)
        {
            std::cerr << "Maze::load => Bad formatting in level data..." << std::endl;
            return false;
        }

        Coord::m_nb_col = this->m_col;
        for (unsigned int i=0; i<lines.size(); i++)
        {
            //LDebug << "Maze::load => Reading : " << lines[i];
            for (unsigned int j=0; j<this->m_col; j++)
            {
                if (j < lines[i].size())
                {
                    bool both = false;
                    unsigned short pos = Coord::coord1D(i, j);
                    unsigned char s = (unsigned char)(lines[i][j] - '0');

                    // Need to add a goal and a box ;)
                    if (s == SPRITE_BOX_PLACED)
                    {
                       both = true;
                    }

                    if (s == SPRITE_GOAL || both)
                    {
                        this->m_pos_goals.push_back(pos);
                    }
                    if (s == SPRITE_BOX || both)
                    {
                        tmpPosBoxes.push_back(pos);
                    }

                    // Assign player position
                    if (s == SPRITE_MARIO)
                    {
                        this->m_pos_player = pos;
                        //LDebug << "\tAdding player pos (" << pos << ")";
                        s = SPRITE_GROUND;
                    }

                    // Add this value in the field
                    this->m_field.push_back(s);
                }
                else
                {
                    // Here - Out of bound
                    this->m_field.push_back(SPRITE_GROUND);
                }
            }
        }

        // Copy box position
        this->m_pos_boxes.resize(tmpPosBoxes.size());
        for (unsigned int i=0; i<tmpPosBoxes.size(); ++i)
        {
            this->m_pos_boxes[i] = tmpPosBoxes[i];
        }

        return (this->m_pos_boxes.size() == this->m_pos_goals.size());
    }
    else
    {
        std::cerr << "Maze::load => File does not exist..." << std::endl;
    }

    return false;
}

void  Maze::bruteForce(Maze m, Graphic g)
{
    bool resolu = false;
    int mvt = 0;
    int cmp = 0;
    bool trouve = true;

    std::vector<unsigned short> position_boxes;
        for (unsigned short i=0; i<m.m_pos_boxes.size(); i++)
        {
                position_boxes.push_back(m.m_pos_boxes[i]);
                //std::cout<<position_boxes[i];
        }

    while(!resolu&&trouve)
    {
        bool condition =true;


        mvt++;

        int *tab = new int[mvt];
        //std::cout<<sizeof(tab);
        //tab = new int* (mvt);
        for(int i=0; i<mvt; i++)
        {
            tab[i]=0;

        }

        while(condition)
        {

            //std::cout<<std::endl;

            resolu = m.reinit(m, g, tab, mvt);



            condition = false;
            for(unsigned short k=0; k<mvt; k++)
            {

                //std::cout<<tab[k];

                if(tab[k]!=3)condition = true;

                //if (condition == false )std::cout<<"done";
            }

            if (!resolu)
            {
                    tab = m.chgtEtatBruteForce(tab, mvt);
                    cmp++;

            }
            if (m._isCompleted())condition = false;
            if(g.keyGet(KEY_ESC))
            {
                condition =false;
                trouve = false;
            }

        }

    if (resolu)
    {
        m.afficherBruteForce(m, g, tab, mvt);
        std ::cout<<"Nombre de combinaisons testees : "<<cmp<<std::endl;
        std::cout<<"Nombre de mouvements necessaires : "<<mvt<<std::endl;
    }

    }
    if(!trouve)rest(1000);


}
void Maze::afficherBruteForce(Maze m, Graphic g, int* tab,int mvt)
{

    for(unsigned short j=0; j<mvt; j++)
            {


                rest(500);
                //Chaque num�ro correspond � un d�placement de Mario
                //0 = Haut
                //1 = Bas
                //2 = Gauche
                //3 = Droite
                if(tab[j]==0)m.updatePlayer(TOP);
                if(tab[j]==1)m.updatePlayer(BOTTOM);
                if(tab[j]==2)m.updatePlayer(LEFT);
                if(tab[j]==3)m.updatePlayer(RIGHT);
                g.clear();
                m.draw(g);
                g.display(Coord::m_nb_col);

            }
            //std::cout<<std::endl;


}

void Maze::detectionDeadLock()
{
    unsigned int cpt(0); //compteur pour la 2�me boucle

    for (unsigned int i=0; i<m_field.size(); i++) /// pour toutes les cases
    {
        if (m_field[i]==SPRITE_GROUND) // si on est sur un sol
            {
            if (m_field[i-1]==SPRITE_WALL) // s'il y a un mur � gauche alors deadlock
            {
                m_field[i] = SPRITE_DEADSQUARE;
            }
            else if (m_field[i+1]==SPRITE_WALL) // s'il y a un mur � droite alors deadlock
            {
                m_field[i] = SPRITE_DEADSQUARE;
            }
            else if (m_field[i-Coord::m_nb_col]==SPRITE_WALL) // s'il y a un mur en haut alors deadlock
            {
                m_field[i] = SPRITE_DEADSQUARE;
            }
            else if (m_field[i+Coord::m_nb_col]== SPRITE_WALL) // s'il y a un mur en bas alors deadlock
            {
                m_field[i] = SPRITE_DEADSQUARE;
            }
            }

    if ( (m_field[i]==SPRITE_DEADSQUARE&&m_field[i+1]==SPRITE_WALL&&m_field[i-1]==SPRITE_WALL) || (m_field[i]==SPRITE_DEADSQUARE&&m_field[i+Coord::m_nb_col]==SPRITE_WALL && m_field[i-Coord::m_nb_col]==SPRITE_WALL))
        {
          m_field[i]=SPRITE_GROUND; //Si une case est au milieu de deux murs (m�me ligne ou m�me colonne) alors ce n'est pas un deadlock
        }
    } // fin de la premi�re boucle qui traitait case par case


    while (cpt<m_field.size()) ///compteur
    {
        for (unsigned int i=0; i<m_field.size(); i++) //pour chaque field
        {

            if(m_field[i] == SPRITE_DEADSQUARE) //Si la case peut �tre pouss�e sur une case non deadlock alors c'est pas un deadlock
            {
                //on v�rifie pour les 4 cas (situ� � droite du deadlock, � gauche, en haut et en bas)
                if( (m_field[i+1]==SPRITE_GROUND || m_field[i+1]==SPRITE_GOAL || m_field[i+1]==SPRITE_BOX) && m_field[i-1]!=SPRITE_WALL)
                {
                    m_field[i] = SPRITE_GROUND;
                }
                else if ( (m_field[i-1]==SPRITE_GROUND ||  m_field[i-1] == SPRITE_GOAL || m_field[i-1]==SPRITE_BOX) && m_field[i+1]!=SPRITE_WALL)
                {
                    m_field[i] = SPRITE_GROUND;
                }
                 else if((m_field[i-Coord::m_nb_col]==SPRITE_GROUND || m_field[i-Coord::m_nb_col] == SPRITE_GOAL || m_field[i-Coord::m_nb_col] == SPRITE_BOX) && m_field[i+Coord::m_nb_col]!=SPRITE_WALL)
                {
                    m_field[i] = SPRITE_GROUND;
                }
                else if( (m_field[i+Coord::m_nb_col]==SPRITE_GROUND || m_field[i+Coord::m_nb_col] == SPRITE_GOAL || m_field[i+Coord::m_nb_col] == SPRITE_BOX) && m_field[i-Coord::m_nb_col]!=SPRITE_WALL)
                {
                    m_field[i] = SPRITE_GROUND;
                }
            }
            }
        cpt++; //on incr�mente le compteur
    }

    for (unsigned i=0;i<m_lig*m_col;i++)
    {
        if (m_field[i]==SPRITE_DEADSQUARE) //si c'est un deadlock on l'ajoute
        {
            m_deadlock.push_back(i);
        }
    }
}


bool Maze::reinit(Maze m, Graphic g, int* tab, int mvt)//Fonction qui remet les caisses et Mario � leur place de d�part
    {

       for(unsigned short j=0; ((j<mvt)&&!m._isCompleted()); j++)
                {

                  std::cout<<tab[j];

                    if(tab[j]==0)m.updatePlayer(TOP);
                    if(tab[j]==1)m.updatePlayer(BOTTOM);
                    if(tab[j]==2)m.updatePlayer(LEFT);
                    if(tab[j]==3)m.updatePlayer(RIGHT);
                    /*g.clear();
                    m.draw(g);
                    g.display(Coord::m_nb_col);*/

                }
                std::cout<<std::endl;
                if (m._isCompleted())return true;
                else return false;

    }


int * Maze::chgtEtatBruteForce(int * tab, int mvt)
{///Cette fonction permet d'obtenir toutes les combinaisons possibles parmi mvt nombre en base 4
    //recoit en parametre un pointeur sur entier qui est en fait un tableau de taille mvt

    int taille =mvt;
    //Cette algorithme reprend le principe du binaire qu'on reprend avec une base 4
    bool condition = true;
    while(condition)
    {
        tab[taille-1]++; //On commence par incr�menter la derniere case du tableau
        if (tab[taille-1]==4)
        { //Lorsque celle-ci vaut 4 on la remet � 0 et on incr�mente la pr�c�dente
            //Si la pr�c�dente vaut 4 on effetue la m�me op�ration et ainsi de suite
            tab[taille-1] = 0;
            if (taille!=0)taille=taille-1;
        }
        else
        {

            condition = false;
        }
    }
    return tab;
    //On retourne ensuite le tableau qui donne l'�tat suivant combinaisons de d�placement

    ///Exemple : Pour un tableau de taille 2 on aura :
    //00; 01; 02; 03; 10; 11; 12; ...; 32; 33


}
bool Maze::updatePlayer(char dir)//Fonction permettant le d�placement de Mario ainsi que le d�placemment des caisses pouss�es par le joueur
{
    //std::cout<<dir;
    if (dir < 0 || dir > MAX_DIR)
    {
        std::cerr << "Maze::updatePlayer => Direction not correct... " << +dir << std::endl;
        return false;
    }

      else
        {
            if(isSquareWalkable(Coord::getDirPos(m_pos_player, dir))||isSquareDeadlock(Coord::getDirPos(m_pos_player, dir)))
                {
                    setPlayerPos(Coord::getDirPos(m_pos_player, dir));
                    m_dir_player=dir;
                }
          else if(isSquareBox(Coord::getDirPos(m_pos_player, dir))  )
    {

        for(unsigned int i=0; i<m_pos_boxes.size(); i++)
            if(Coord::getDirPos(m_pos_player, dir)== m_pos_boxes[i]  && isSquareWalkable(Coord::getDirPos(m_pos_boxes[i], dir)))
            {
                setPlayerPos(Coord::getDirPos(m_pos_player, dir));
                m_dir_player=dir;
                if(isSquareBoxPlaced( Coord::getOppositeDirPos(Coord::getDirPos(m_pos_player, dir) ,  dir )))
                    setSquare(m_pos_boxes[i],SPRITE_GOAL );
                else
                    setSquare(m_pos_boxes[i], SPRITE_GROUND);
                if(isSquareGoal(Coord::getDirPos(m_pos_boxes[i], dir)))
                {
                    m_pos_boxes[i]=Coord::getDirPos(m_pos_boxes[i], dir);
                    setSquare(m_pos_boxes[i], SPRITE_BOX_PLACED);
                }
                else
                {
                    m_pos_boxes[i]=Coord::getDirPos(m_pos_boxes[i], dir);
                    setSquare(m_pos_boxes[i], SPRITE_BOX);
                }

            }
    }
      }

    // Implement player moving and pushing here

    return false;
}

void Maze::Bfs(Maze m, Graphic g)
{
    bool trouve = true;

    std::vector <Sommet> graphe; //graphe qui contient tous les �tats trouv�s lors de la recherche du plus cours chemin
    graphe.push_back(Sommet(-1,0,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
    graphe[graphe.size()-1].setNumber(graphe.size()-1); //Num�ro de l'�tat correspondant � son emplacement dans le tableau dynamique

    std::queue <Sommet> file;
    file.push(graphe[0]);// On initialise la file aux 1er �tat = position de base du Mario


    bool resolu = false;//Bool�en de fin de niveau

    while (!resolu)
    {

        Sommet temp = file.front(); //On r�cupere le 1er �l�ment de la file
        file.pop();// Qu'on supprime ensuite pour le marquer car celui-ci est pr�sent dans graphe

        //on initialise notre carte au caract�ristque de temp =1er �l�ment de file
        m.m_pos_boxes = temp.getPosBoxes();
        m.m_field = temp.getMap();
        m.m_pos_player = temp.getPosPlayer();

        for(int i = 0; i<4&&!resolu; i++)
        {
            if(g.keyGet(KEY_ESC))
            {
                resolu = true;
                trouve = false;
            }
            if(!resolu)
            {
                m.updatePlayer(i);
                if(m._isCompleted())resolu = true; //On regarde si apr�s le d�placement el niveau est compl�t�
                //std::cout<<resolu;

                if(!resolu) // Si "non"
                {
                        bool existe = false;
                        for(int j=graphe.size()-1; j>=0; j--)
                            {
                                if(graphe[j].getPosPlayer()==m.m_pos_player && graphe[j].getPosBoxes()==m.m_pos_boxes)
                                    {
                                        existe=true; // On verifie que l'etat qu'on a trouv� n'existe pas encore dans l'arbre
                                    }

                            }
                        if (!existe) //On retient l'�tat si celui-ci n'exsite pas encore
                            {

                                graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                                graphe[graphe.size()-1].setNumber(graphe.size()-1);
                                file.push(graphe[graphe.size()-1]); // On push l'�tat en derniere position dans la file
                            }
                        m.m_pos_boxes = temp.getPosBoxes();
                        m.m_field = temp.getMap();
                        m.m_pos_player = temp.getPosPlayer();

                }
                else  //Si le niveau est r�solu
                {
                    graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                    graphe[graphe.size()-1].setNumber(graphe.size()-1);

                    //On remet tous Mario et les boites a leur place
                    m.m_pos_boxes = graphe[0].getPosBoxes();
                    m.m_field = graphe[0].getMap();
                    m.m_pos_player = graphe[0].getPosPlayer();


                }
            }
        }
        //cmp++;
        //std::cout<<cmp;
    }

    if (trouve)
    {


    std::cout<<"Nombre de sommets : "<<graphe.size()<<std::endl;
    std::stack <unsigned short> tab;
    int j = graphe.size()-1;

    while(graphe[j].getPred()!=-1)// On part du dernier �tat de graphe qui correspond � l'�tat r�solu
    {
        tab.push(graphe[j].getDepPred()); //on r�cup�re chaque d�placement qu'on mets les un a la suite des autres dans un tableau
        j = graphe[j].getPred();// on remonte le chemin en partant de la fin grace au pr�decesseurs
    }



    std::vector<unsigned short> chemin;
        while(!tab.empty())
    {
        chemin.push_back(tab.top());//On remet le chemin dans le bon sens
        tab.pop();
    }
    std::cout<<"Profondeur de la solution : "<<chemin.size()<<std::endl;
    m.AfficherBfsDfs(g, chemin); //on envoie la solution pour l'affichage
    }
    else{rest (1000);}


}
void Maze::Dfs(Maze m, Graphic g)
{

    std::vector <Sommet> graphe; //graphe qui contient tous les �tats trouv�s lors de la recherche du plus cours chemin
    graphe.push_back(Sommet(-1,0,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
    graphe[graphe.size()-1].setNumber(graphe.size()-1); //Num�ro de l'�tat correspondant � son emplacement dans le tableau dynamique

    std::stack <Sommet> pile;
    pile.push(graphe[0]);// On initialise la pile aux 1er �tat = position de base du Mario

    bool trouve = true;

    bool resolu = false;//Bool�en de fin de niveau

    while (!resolu)
    {

        Sommet temp = pile.top(); //On r�cupere le 1er �l�ment de la pile
        pile.pop();// Qu'on supprime ensuite pour le marquer car celui-ci est pr�sent dans graphe

        //on initialise notre carte au caract�ristque de temp =1er �l�ment de pile
        m.m_pos_boxes = temp.getPosBoxes();
        m.m_field = temp.getMap();
        m.m_pos_player = temp.getPosPlayer();

        for(int i = 0; i<4&&!resolu; i++)
        {
            if(g.keyGet(KEY_ESC))
            {
                resolu = true;
                trouve = false;
            }
            if(!resolu)
            {
                m.updatePlayer(i);
                if(m._isCompleted())resolu = true; //On regarde si apr�s le d�placement el niveau est compl�t�
                //std::cout<<resolu;

                if(!resolu) // Si "non"
                {
                        bool existe = false;
                        for(int j=graphe.size()-1; j>=0; j--)
                            {
                                if(graphe[j].getPosPlayer()==m.m_pos_player && graphe[j].getPosBoxes()==m.m_pos_boxes)
                                    {
                                        existe=true; // On verifie que l'etat qu'on a trouv� n'existe pas encore dans l'arbre
                                    }

                            }
                        if (!existe) //On retient l'�tat si celui-ci n'exsite pas encore
                            {

                                graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                                graphe[graphe.size()-1].setNumber(graphe.size()-1);
                                pile.push(graphe[graphe.size()-1]); // On push l'�tat en premiere position dans la file
                            }
                        m.m_pos_boxes = temp.getPosBoxes();
                        m.m_field = temp.getMap();
                        m.m_pos_player = temp.getPosPlayer();

                }
                else  //Si le niveau est r�solu
                {
                    graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                    graphe[graphe.size()-1].setNumber(graphe.size()-1);

                    //On remet tous Mario et les boites a leur place
                    m.m_pos_boxes = graphe[0].getPosBoxes();
                    m.m_field = graphe[0].getMap();
                    m.m_pos_player = graphe[0].getPosPlayer();


                }
            }
        }
        //cmp++;
        //std::cout<<cmp;
    }

    if (trouve)
    {


    std::cout<<"Nombre de sommets : "<<graphe.size()<<std::endl;
    std::stack <unsigned short> tab;
    int j = graphe.size()-1;

    while(graphe[j].getPred()!=-1)// On part du dernier �tat de graphe qui correspond � l'�tat r�solu
    {
        tab.push(graphe[j].getDepPred()); //on r�cup�re chaque d�placement qu'on mets les un a la suite des autres dans un tableau
        j = graphe[j].getPred();// on remonte le chemin en partant de la fin grace au pr�decesseurs
    }



    std::vector<unsigned short> chemin;
        while(!tab.empty())
    {
        chemin.push_back(tab.top());//On remet le chemin dans le bon sens
        tab.pop();
    }
    std::cout<<"Profondeur de la solution : "<<chemin.size()<<std::endl;
    m.AfficherBfsDfs(g, chemin); //on envoie la solution pour l'affichage
    }
    else{rest(1000);}


}


void Maze::AfficherBfsDfs(Graphic g, std::vector<unsigned short> chemin)
{//Affichage des solutions obtenues par les algo BFS, DFS, BestFS.
        for(int i=0; i<chemin.size(); i++)
    {
        rest(500);

        updatePlayer(chemin[i]);
        g.clear();
        draw(g);
        g.display(Coord::m_nb_col);
    }

}

void Maze::BestFirstChoice(Maze m, Graphic g)
{//On utilisela m�me technique que pour le BFS sauf sur le choix du Noeud suivant ou on utilise l'heuristique

    std::vector <Sommet> graphe;
    graphe.push_back(Sommet(-1,0,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
    graphe[graphe.size()-1].setNumber(graphe.size()-1);

    std::vector <Sommet> noeudSuivant;
    noeudSuivant.push_back(graphe[0]);
    bool trouve = true;
    int mvt = 0;

    bool resolu = false;//Bool�en de fin de niveau

    while (!resolu)
    {

        int j = 0;
        int i = 0;

        int pred = noeudSuivant[j].getPred();
        //C'est dans ce while que le choix du Noeud suivant s'effectue

        while((noeudSuivant[j].getPred()==pred)&&(j<noeudSuivant.size()))
        {//On cherche dans le vector noeudSuivant parmi les �tats de m�me pr�decesseur
            mvt++;
            noeudSuivant[j].setCout(heuristique(noeudSuivant[j]));
            if (noeudSuivant[j].getCout()<noeudSuivant[i].getCout())
            { //l'Etat ayant le co�t minimum
                //Ce co�t est d�fini par la fonction heuristqiue
                i=j;
            }
            j++;
        }



        Sommet temp = noeudSuivant[i];
        noeudSuivant.erase(noeudSuivant.begin()+i);

        m.m_pos_boxes = temp.getPosBoxes();
        m.m_field = temp.getMap();
        m.m_pos_player = temp.getPosPlayer();

        //std::cout<<noeudSuivant.size();

        for(int i = 0; i<4; i++)
        {
            if(g.keyGet(KEY_ESC))
            {
                resolu = true;
                trouve = false;
            }
            if(!resolu)
            {
                m.updatePlayer(i);
                if(m._isCompleted())resolu = true;

                if(!resolu)
                {
                        bool existe = false;
                        for(int j=graphe.size()-1; j>0; j--)
                            {
                                if(graphe[j].getPosPlayer()==m.m_pos_player && graphe[j].getPosBoxes()==m.m_pos_boxes)
                                    {
                                        existe=true; // On verifie que l'etat qu'on a trouv� n'existe pas encore dans l'arbre
                                    }
                            }
                        if (!existe)
                            {
                                //std::cout<<" Pred = "<<cmp<<std::endl;
                                //std::cout<<" i : "<<i<<std::endl;
                                graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                                graphe[graphe.size()-1].setNumber(graphe.size()-1);
                                noeudSuivant.push_back(graphe[graphe.size()-1]);
                            }
                        m.m_pos_boxes = temp.getPosBoxes();
                        m.m_field = temp.getMap();
                        m.m_pos_player = temp.getPosPlayer();

                }
                else if(resolu) //Si le niveau est r�solu
                {
                    graphe.push_back(Sommet(temp.getNumber(),i,m.getPosPlayer(),m.getPosBoxes(),m.getField()));
                    graphe[graphe.size()-1].setNumber(graphe.size()-1);


                    //On remet tous Mario et les boites a leur place
                    m.m_pos_boxes = graphe[0].getPosBoxes();
                    m.m_field = graphe[0].getMap();
                    m.m_pos_player = graphe[0].getPosPlayer();


                }
            }
        }


    }
    if (trouve)
    {


    std::cout<<"Nombre de sommets : "<<graphe.size()<<std::endl;


    std::stack <unsigned short> tab;
    int j = graphe.size()-1;


    while(graphe[j].getPred()!=-1)
    {

        tab.push(graphe[j].getDepPred());
        j = graphe[j].getPred();
    }

    std::vector<unsigned short> chemin;
        while(!tab.empty())
    {
        chemin.push_back(tab.top());
        tab.pop();
    }
    std::cout<<"Profondeur de la solution : "<<chemin.size()<<std::endl;
    m.AfficherBfsDfs(g, chemin);
    }
    else {rest(1000);}

}
int Maze::heuristique(Sommet s)
{

    ///Notre choix d'heuristique est le suivant : on donne comme co�t a chaque noeud la somme des distances entre un box et le goal le plsu proche de celui-ci


    int tabDistance [m_pos_boxes.size()] ;
    int tabGoalMarque[m_pos_goals.size()] ;
    for(int i=0;i<s.getPosBoxes().size(); i++)
        {
            tabDistance[i]=-1;
            tabGoalMarque[i]=-1;
        }

    double dist = 0;
    int distance =0;
    double puissance = 2;
    int somme = 0;


    for(int i=0; i<s.getPosBoxes().size(); i++)
    {
        for(int j=0; i<m_pos_goals.size(); i++)
            {

                unsigned int l1 = 0;
                unsigned int c1 = 0;
                unsigned int l2 = 0;
                unsigned int c2 = 0;

                Coord::coord2D(m_pos_goals[j], l1, c1);
                Coord::coord2D(s.getPosBoxes()[i], l2, c2);

                double ll1, ll2, cc1, cc2;
                ll1 = l1;
                ll2 = l2;
                cc1 = c1;
                cc2 = c2;


                ///Distance "� vol d'oiseau"
                //dist = sqrt(pow((ll2-ll1), puissance)+pow((cc2-cc1), puissance));

                ///Distance r�ellement parcouru par Mario lorsqu'il pousse la caisse
                dist = abs(ll2-ll1)+abs(cc2-cc1);

                distance = (int) dist;
                if(tabDistance[i]==-1)
                {

                    tabGoalMarque[i] = j;

                    tabDistance[i]=distance;

                }

                else
                    {

                        for(int k=0;k<s.getPosBoxes().size(); k++)
                            {
                                if(j==tabGoalMarque[k])
                                {

                                    if(distance<tabDistance[k])
                                    {
                                        tabGoalMarque[i] = j;
                                        tabDistance[i] = distance;
                                        i = k-1;
                                    }
                                }
                            }
                }

            }
    }
    for (int i=0; i<m_pos_goals.size(); i++)
    {
        somme = somme + tabDistance[i];
    }
    return somme;
    //On retourne la somme des distances Caisse-Goal

}


// Display maze on screen with Allegro
void Maze::draw(const Graphic& g) const
{
    for(unsigned int i=0; i<this->getSize(); i++)
    {
        unsigned int l = 0, c = 0;
        Coord::coord2D(i, l, c);

        if (i == this->m_pos_player)
        {
            g.drawT(g.getSpritePlayer(this->m_dir_player), c, l);
        }
        else
        {
            g.drawT(g.getSprite(this->m_field[i]), c, l);
        }
    }
}


// DO NOT TOUCH !
// Overload function for displaying debug information
// about Maze class
std::ostream& operator << (std::ostream& O, const Maze& m)
{
    unsigned int l, c;
    int i = 0;
    Coord::coord2D(m.m_pos_player, l, c);
    O << "Player position " << m.m_pos_player << " (" << l << "," << c << ")" << std::endl;
    O << "Field Size " << +m.m_lig << " x " << +m.m_col << " = " << m.getSize() << std::endl;
    O << "Field Vector capacity : " << m.m_field.capacity() << std::endl;
    O << "Field array : " << std::endl << std::endl;
    for(unsigned int l=0; l<m.getSize(); l++)
    {
        if (l == m.m_pos_player) Console::getInstance()->setColor(_COLOR_YELLOW);
        else if (m.isSquareWall(l)) Console::getInstance()->setColor(_COLOR_PURPLE);
        else if (m.isSquareBoxPlaced(l) || m.isSquareGoal(l)) Console::getInstance()->setColor(_COLOR_GREEN);
        else if (m.isSquareBox(l)) Console::getInstance()->setColor(_COLOR_BLUE);
        else if (m.m_field[l] == SPRITE_DEADSQUARE) Console::getInstance()->setColor(_COLOR_RED);
        else Console::getInstance()->setColor(_COLOR_WHITE);

        O << std::setw(2) << +m.m_field[l] << " "; // + => print as "int"

        if ((l+1) % m.m_col == 0)
        {
            O << std::endl;
        }
    }
    Console::getInstance()->setColor(_COLOR_DEFAULT);

    O << std::endl;
    O << "Box position : " << std::endl;
    for (unsigned int i=0; i<m.m_pos_boxes.size(); i++)
    {
        Coord::coord2D(m.m_pos_boxes[i], l, c);
        O << "\t" << "Box #" << i << " => " << std::setw(3) << m.m_pos_boxes[i] << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
    }

    O << std::endl;
    O << "Goal position : " << std::endl;
    for (const auto& goal : m.m_pos_goals)
    {
        unsigned int l, c;
        Coord::coord2D(goal, l, c);
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_GREEN);
        O << "\t" << "Goal #" << i << " => " << std::setw(3) << goal << std::setw(2) << " (" << l << "," << c << ")" << std::endl;
        if (m.isSquareBoxPlaced(goal)) Console::getInstance()->setColor(_COLOR_DEFAULT);
        i++;
    }

    return O;
}
