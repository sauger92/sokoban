#ifndef NOEUD_H_INCLUDED
#define NOEUD_H_INCLUDED
#include <vector>


class Maze;
class Graphic;
class Noeud
{
    public:
        Noeud();
        Noeud(unsigned short pos, int dep);
        virtual ~Noeud();
        unsigned short Getpred() { return m_pred; }
        void Setpred(unsigned short val) { m_pred = val; }
        std::vector <unsigned short> Getsucc() { return m_succ; }
        void Setsucc(std::vector <unsigned short> val) { m_succ = val; }
        std::vector <unsigned short> Getpos_actuel_player() { return m_pos_actuel_player; }
        void Setpos_actuel_player(std::vector <unsigned short> val) { m_pos_actuel_player = val; }
        std::vector <unsigned short> Getpos_actuel_boxes() { return m_pos_actuel_boxes; }
        void Setpos_actuel_boxes(std::vector <unsigned short> val) { m_pos_actuel_boxes = val; }
        int Getdep() { return m_dep; }
        void Setdep(int val) { m_dep = val; }
        unsigned short GetposPlayer() { return m_posPlayer; }
        void SetposPlayer(unsigned short val) { m_posPlayer = val; }

        int * BFS(Noeud n, Maze m, Graphic g);
        std::vector< std::vector<int>> deplacement_Mario (Maze m );

    private:
        unsigned short m_pred;
        std::vector <unsigned short> m_succ;
        std::vector <unsigned short> m_pos_actuel_player;
        std::vector <unsigned short> m_pos_actuel_boxes;
        unsigned short m_posPlayer;
        int m_dep;
        bool marquage;
};



#endif // NOEUD_H_INCLUDED
