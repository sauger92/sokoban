#ifndef SOMMET_H
#define SOMMET_H
#include <vector>

class Sommet ///Cette class correspond a un �tat donn� d'un niveau
{
    public:
        Sommet();
        Sommet(int pred,unsigned short depPred,unsigned short player,std::vector <unsigned short> boxes,
        std::vector<unsigned char> Map);
        virtual ~Sommet();

        //getter
        int getPred(){return m_pred;}
        int getCout(){return m_cout;}
        bool getMarque(){return m_marque;}
        unsigned short getDepPred(){return m_depPred;}
        unsigned short getPosPlayer(){return m_pos_actuel_player;}
        std::vector <unsigned short> getPosBoxes(){return m_pos_actuel_boxes;}
        std::vector<unsigned char>getMap(){return m_map;}
        int getNumber(){return m_number;}


        //setter
         void setPred(int pred){m_pred = pred;}
         void setCout(int Cout){m_cout = Cout;}
         void setMarque(bool marque){m_marque = marque;}
         void setDepPred(unsigned short depPred){m_depPred=depPred;}
         void setPosActuelPlayer(unsigned short player){m_pos_actuel_player =player;}
         void setPosActuelBoxes(std::vector <unsigned short> boxes){m_pos_actuel_boxes = boxes;}
         void setMap(std::vector<unsigned char> Map){m_map = Map;}
         void setNumber(int number){m_number = number;}



    protected:
    private:

        int m_pred; //Num�ro du pr�decesseur
        int m_cout; //Cout donn� par l'heuristique
        bool m_marque;
        unsigned short m_depPred; //Deplacement de Mario dans cette �tat
        unsigned short m_pos_actuel_player; //Position de Mario
        std::vector <unsigned short> m_pos_actuel_boxes; //Position des caisses
        std::vector<unsigned char> m_map;
        int m_number; //Num�ro de l'�tat

};

#endif // SOMMET_H
