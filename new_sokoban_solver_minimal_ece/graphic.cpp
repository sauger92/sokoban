/************************************************************
Sokoban project - Graphic file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/

#include "graphic.h"

#include "utils/coord.h"
#include <string>
#include "stdio.h"
#include "stdlib.h"
#include <iostream>


Graphic::Graphic(int _resX, int _resY, int _depth)
    : pBuffer(NULL), m_resX(_resX), m_resY(_resY), m_depth(_depth)
{
    for (int i=0; i<MAX_SPRITE; i++)
        pSprites[i] = NULL;
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
        pSpritesPlayer[i] = NULL;
}

Graphic::~Graphic()
{

}

std::string Graphic::menu(Graphic g) //Choix du niveau par l'utilisateur
{
    //BITMAP *fondmenu;
    //Menu0 : ;

    FONT *myfont;

    PALETTE palette;

    myfont = load_font("prestige1.pcx", palette, NULL);//charge le fichier font.pcx contenant la police
    if (!myfont)
    { {}
    allegro_message("Chargement de la police �chou� !\nV�rifiez le dossier fonts");
    exit(EXIT_FAILURE);
    }
    g.pBuffer = imageCreate(m_resX, m_resY, COLOR_WHITE);
    if (!g.pBuffer)
    {
        std::cerr << "Cannot allocate buffer..." << std::endl;
        return 0;
    }



    int choix =0;
    int choix1=0;
    bool retour = false;
    std::string chaine;





    while (choix1==0)
    {
        if (choix == 0 || retour)
        {

            choix = 0;
            choix1 = 0;



        g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "EASY" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "MEDIUM" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "HARD" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "TEST" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "QUITTER" );
        //g.displayMenu(Coord::m_nb_col);


        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "EASY" );
            if(mouse_b & 1)
            {
                choix = 1;
                rest(1000);
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "MEDIUM" );
            if(mouse_b & 1)
            {
                choix = 2;
                rest(1000);
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "HARD" );
           if(mouse_b & 1)
           {
               choix = 3;
               rest(1000);
           }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "TEST" );
           if(mouse_b & 1)
           {
               choix = 4;
               rest(1000);
           }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "QUITTER" );
            if(mouse_b & 1)
                exit(EXIT_FAILURE);
        }

    //blit(g.pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);

    }






    //choix =1;
    // int choix1 =0;



        if (choix==1)
        {
            g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "easy_1");
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "easy_2" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "easy_3" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "easy_4" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "RETOUR" );

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "easy_1" );
            if(mouse_b & 1)
            {
                choix1 =1;
               chaine="./levels/easy/easy_1.dat";

            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "easy_2" );
            if(mouse_b & 1)
            {
                choix1 = 2;
                chaine="./levels/easy/easy_2.dat";
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "easy_3" );
           if(mouse_b & 1)
           {
               choix1 = 3;
               chaine="./levels/easy/easy_3.dat";
           }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "easy_4" );
           if(mouse_b & 1)
           {
               choix1 = 4;
            chaine="./levels/easy/easy_4.dat";
           }
        }

        /*if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "RETOUR" );
            if(mouse_b & 1)
            {
                retour =true;
                choix1 = 0;
            }

        }*/


        }
        if (choix==2)
        {
            g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "medium_1");
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "medium_2" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "medium_3" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "medium_4" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "RETOUR" );

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+250) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "medium_1" );
            if(mouse_b & 1)
            {
                choix1 = 5;
               chaine="./levels/medium/medium_1.dat";
               //strcpy(copie, chaine);
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+250) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "medium_2" );
            if(mouse_b & 1)
            {
                choix1 = 6;
                chaine = "./levels/medium/medium_2.dat";
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+250) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "medium_3" );
           if(mouse_b & 1)
           {
               choix1 = 7;
              chaine = "./levels/medium/medium_3.dat";
           }
        }

       /* if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "medium_4" );
           if(mouse_b & 1)
           {
               choix1 = 8;
              chaine = "./levels/medium/medium_4.dat";
           }
        }*/

       /* if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "RETOUR" );
            if(mouse_b & 1){
                retour =true;
                choix1 = 0;
            }
        }*/



        }
            if (choix==3)
        {g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "hard_1");
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "hard_2" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "hard_3" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "hard_4" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "RETOUR" );
        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "hard_1" );
            if(mouse_b & 1)
            {
                choix1 = 9;
             chaine = "./levels/hard/hard_1.dat";
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "hard_2" );
            if(mouse_b & 1)
            {
                choix1 = 10;
               chaine = "./levels/hard/hard_2.dat";
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "hard_3" );
           if(mouse_b & 1)
           {
               choix1 = 11;
              chaine = "./levels/hard/hard_3.dat";
           }
        }

        /*if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "hard_4" );
           if(mouse_b & 1)
           {
               choix1 = 12;
             chaine = "./levels/hard/hard_4.dat";
           }
        }*/

    /*if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "RETOUR" );
            if(mouse_b & 1){
                retour =true;
                choix1 = 0;
            }
        }*/



        }
            if (choix==4)
        {
            g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "test_1");
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "test_2" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "test_3" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "test_4" );
        //textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "RETOUR" );
        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "test_1" );
            if(mouse_b & 1)
            {
                choix1 = 13;
                chaine = "./levels/test/test_1.dat";
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "test_2" );
            if(mouse_b & 1)
            {
                choix1 = 14;
                chaine = "./levels/test/test_2.dat";

            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "test_3" );
           if(mouse_b & 1)
           {
               choix1 = 15;
               chaine = "./levels/test/test_3.dat";
           }
        }

        /*if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+150) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "test_4" );
           if(mouse_b & 1)
           {
               choix1 = 16;
            //   chaine = "./levels/test/test_4.dat";
           }
        }*/

        /*if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "RETOUR" );
            if(mouse_b & 1){
                retour =true;
                choix1 = 0;
            }
        }*/

        }
        //blit(g.pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);

    blit(g.pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);


    }



    return chaine;



}
int Graphic::Menu2(Graphic g) //Choix de l'algorithme a utiliser
{
     FONT *myfont;
Menu :
   // char  chaine [];
    int choix =0;
    int choix1=0;

    PALETTE palette;

    myfont = load_font("prestige1.pcx", palette, NULL);//charge le fichier font.pcx contenant la police
    if (!myfont)
    { {}
    allegro_message("Chargement de la police �chou� !\nV�rifiez le dossier fonts");
    exit(EXIT_FAILURE);
    }
    g.pBuffer = imageCreate(m_resX, m_resY, COLOR_WHITE);
    if (!g.pBuffer)
    {
        std::cerr << "Cannot allocate buffer..." << std::endl;
        return 0;
    }


    while (choix==0)
    {


        g.clear();
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(100, 150, 200), -1, "BRUTE FORCE" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+100, makecol(100, 150, 200), -1, "BFS" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+200, makecol(100, 150, 200), -1, "DFS" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+300, makecol(100, 150, 200), -1, "BestFirstChoice" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(100, 150, 200), -1, "JOUER" );
        textprintf_ex(g.pBuffer, myfont, pBuffer->w-200, pBuffer->h/10+450, makecol(100, 150, 200), -1, "RETOUR" );
        //g.displayMenu(Coord::m_nb_col);

        //Lorsque l'utilisateur met la souris sur un des choix celui-ci se colore en jaune!
        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+320) && (mouse_y >= pBuffer->h/10 && mouse_y < pBuffer->h/10+40))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10, makecol(255, 255, 0), -1, "BRUTE FORCE" );
            if(mouse_b & 1) //Coresspond � un clic droit de la souris
            {
                choix = 1;
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+80) && (mouse_y >= pBuffer->h/10+100 && mouse_y < pBuffer->h/10+140))
        {
            textprintf_ex(g.pBuffer, myfont,pBuffer->w/10, pBuffer->h/10+100, makecol(255, 255, 0), -1, "BFS" );
            if(mouse_b & 1)
            {
                choix = 2;
            }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+80) && (mouse_y >= pBuffer->h/10+200 && mouse_y < pBuffer->h/10+240))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+200, makecol(255, 255, 0), -1, "DFS" );
           if(mouse_b & 1)
           {
               choix = 3;
           }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+400) && (mouse_y >= pBuffer->h/10+300 && mouse_y < pBuffer->h/10+340))
        {
           textprintf_ex(g.pBuffer, myfont,  pBuffer->w/10, pBuffer->h/10+300, makecol(255, 255, 0), -1, "BestFirstChoice" );
           if(mouse_b & 1)
           {
               choix = 4;
           }
        }

        if((mouse_x >= pBuffer->w/10 && mouse_x < pBuffer->w/10+200) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w/10, pBuffer->h/10+450, makecol(255, 255, 0), -1, "JOUER" );
            if(mouse_b & 1) choix = 5;
                //exit(EXIT_FAILURE);
        }
        if((mouse_x >= pBuffer->w-200 && mouse_x < pBuffer->w) && (mouse_y >= pBuffer->h/10+450 && mouse_y < pBuffer->h/10+490))
        {
            textprintf_ex(g.pBuffer, myfont, pBuffer->w-200, pBuffer->h/10+450, makecol(255, 255, 0), -1, "RETOUR" );
            if(mouse_b & 1) choix = 6;
        }


    blit(g.pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);

    }

    return choix;
}

bool Graphic::_imageLoadSprites()
{
    this->pBuffer = imageCreate(m_resX, m_resY, COLOR_WHITE);
    if (!this->pBuffer)
    {
        std::cerr << "Cannot allocate buffer..." << std::endl;
        return 0;
    }

    // chargement des bitmaps du jeu
    this->pSprites[0] = imageCreate(TX, TY, COLOR_WHITE);
    this->pSprites[1] = imageLoad("images/mur.bmp");
    this->pSprites[2] = imageLoad("images/caisse.bmp");
    this->pSprites[3] = imageLoad("images/caisse_ok.bmp");
    this->pSprites[4] = imageLoad("images/objectif.bmp");
    this->pSprites[5] = imageCreate(TX, TY, COLOR_ROSE);
    this->pSprites[6] = imageCreate(TX, TY, COLOR_BLUE);
    this->pSprites[7] = imageCreate(TX, TY, COLOR_YELLOW);
    this->pSprites[8] = imageCreate(TX, TY, COLOR_GREEN);
    this->pSprites[9] = imageCreate(TX, TY, COLOR_RED); /// cyan -> red

    this->pSpritesPlayer[0] = imageLoad("images/mario_haut.bmp");
    this->pSpritesPlayer[1] = imageLoad("images/mario_bas.bmp");
    this->pSpritesPlayer[2] = imageLoad("images/mario_gauche.bmp");
    this->pSpritesPlayer[3] = imageLoad("images/mario_droite.bmp");

    for (int i=0; i<MAX_SPRITE; i++)
    {
        if (this->pSprites[i] == NULL)
        {
            return false;
        }
    }
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
    {
        if (this->pSpritesPlayer[i] == NULL)
        {
            return false;
        }
    }

    return true;
}

bool Graphic::init()
{
    set_color_depth(m_depth); // profondeur des couleurs 8, 16, 24 ou 32
    set_uformat(U_ASCII); // pour g�rer les accents

    allegro_init();

    if (install_mouse() == -1)
    {
        std::cerr << ("Failed to install mouse") << std::endl;
        return false;
    }
    if (install_keyboard() != 0)
    {
        std::cerr << ("Failed to install keyboard") << std::endl;
        return false;
    }
    if (set_gfx_mode(GFX_AUTODETECT_WINDOWED, m_resX, m_resY, 0, 0) != 0)
    {
        std::cerr << ("Failed to enter in graphic mode") << std::endl;
        return false;
    }

    set_keyboard_rate(500, 100);

    show_mouse(screen);

    return this->_imageLoadSprites();
}

void Graphic::deinit()
{
    for (int i=0; i<MAX_SPRITE; i++)
    {
        imageDestroy(this->pSprites[i]);
    }
    for (int i=0; i<MAX_SPRITE_PLAYER; i++)
    {
        imageDestroy(this->pSpritesPlayer[i]);
    }

    imageDestroy(pBuffer);
    remove_mouse();
    remove_keyboard();
    allegro_exit();
}

pSurface Graphic::imageLoad(const std::string& fileName)
{
    return load_bitmap(fileName.c_str(), NULL);
}

pSurface Graphic::imageCreate(int width, int height, int color)
{
    pSurface s = NULL;
    s = create_bitmap(width, height);
    if (s != NULL)
        imageClearToColor(s, color);

    return s;
}

void Graphic::imageDestroy(pSurface s)
{
    destroy_bitmap(s);
}

void Graphic::imageClearToColor(const pSurface s, int color)
{
    clear_to_color(s, color);
}

void Graphic::clear()
{
    imageClearToColor(pBuffer, COLOR_WHITE);
}

void Graphic::drawCircle(int x, int y, int radius, int color, int fill, bool coord) const
{
    if (coord)
    {
        x = x*TX + TX/2;
        y = y*TY + TY/2;
    }

    if (fill)
        circlefill(pBuffer, x, y, radius, color);
    else circle(pBuffer, x, y, radius, color);
}

void Graphic::drawRect(int x1, int y1, int x2, int y2, int color, int fill, bool coord) const
{
    if (coord)
    {
        x1 *= TX;
        y1 *= TY;
        x2 *= TX;
        y2 *= TY;
    }

    if (fill==1)
        rectfill(pBuffer, x1, y1, x2, y2, color);
    else if (fill > 1)
    {
        rect(pBuffer, x1-1, y1-1, x2+1, y2+1, color);
        rect(pBuffer, x1, y1, x2, y2, color);
        rect(pBuffer, x1+1, y1+1, x2-1, y2-1, color);
    }
    else rect(pBuffer, x1, y1, x2, y2, color);
}

void Graphic::drawT(const pSurface src, int x, int y, bool coord) const
{
    if (coord)
    {
        x *= TX;
        y *= TY;
    }
    draw_sprite(pBuffer, src, x, y);
}

void Graphic::draw(const pSurface src, int source_x, int source_y, int dest_x, int dest_y, int width, int height, bool coord) const
{
    if (coord)
    {
        source_x *= TX;
        source_y *= TY;
        dest_x *= TX;
        dest_y *= TY;
    }
    blit(src, pBuffer, source_x, source_y, dest_x, dest_y, width, height);
}

void Graphic::display(int nbCol, bool withGrid)
{
    if (withGrid) this->_grid(COLOR_BLACK);
    //draw_sprite(screen, pBuffer, 0, 0);

    if (nbCol > 0)
    {
        int col = mouse_x / TX;
        int lig = mouse_y / TY;
        textprintf_ex(pBuffer, font, mouse_x, mouse_y, COLOR_BLUE, -1,
            "Pos = %d", (lig * nbCol + col));
    }

    blit(pBuffer, screen, 0, 0, 0, 0, pBuffer->w, pBuffer->h);

    rest(20); // tempo
}



void Graphic::cursor(int l, int c, int color)
{
    drawRect(c, l, (c+1), (l+1), color, 0);
}

void Graphic::_grid(int color)
{
    int nbL = 25; //pBuffer->w/TX;
    int nbC = pBuffer->h/TY;
    for(int i=0; i<nbL; i++)
        for(int j=0; j<nbC; j++)
            drawRect(i, j, (i+1), (j+1), color, 0);
}

