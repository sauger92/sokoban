#include "maze.h"
#include "graphic.h"
#include "noeud.h"
#include "utils/coord.h"
#include "utils/console.h"
#include <stack>
#include <queue>


Noeud::Noeud()
{
    //ctor
}

Noeud::Noeud(unsigned short pos, int dep)
{
    m_dep =dep;
    m_posPlayer =pos;
}

Noeud::~Noeud()
{
    //dtor
}


std::vector< std::vector<int>> Noeud::deplacement_Mario (Maze m ) /// fonction qui permet de savoir o� mario peut aller (appel�e � chaque tour de BFS)
{
    ///D�claration
    std::vector<std::vector<int>> mat_mario;
    std::queue<int> file;
    unsigned int l =0;
    unsigned int c=0;
    unsigned int &refL = l;
    unsigned int &refC = c;
    int tailleC=0;
    int x=0;
    unsigned short m_player = m.getPosPlayer();

   ///Initialisation
    Coord::coord2D(m_player, refC, refL);
    for (unsigned int i=0; i<l; i++)
    {
        for (unsigned int j=0; j<c; j++)
        {
            mat_mario[i][j] = 0;
        }
    }
    file.push(m_player); //D�part

    ///D�but de l'algo
    while (!file.empty()) //Tant que la file n'est pas vide
    {
        x = file.front();
        file.pop(); //on enl�ve le noeud
        l = (x/ tailleC);
        c = x - (tailleC*l);
        mat_mario[l][c]=1;
        int dir=0;
        unsigned short mouvement=0;

        while (dir<4) //tant que toutes les directions n'ont pas �t� test�es
        {
            switch (dir)
            {
                case 0:
                    mouvement = -Coord::m_nb_col;
                    break;
                case 1:
                    mouvement = Coord::m_nb_col;
                    break;
                case 2:
                    mouvement = -1;
                    break;
                case 3:
                    mouvement = +1;
                    break;
            }

        }

            if ( mat_mario[l][c] != 0 && m.isSquareWalkable(x)) //si pas marqu� et si ce n'est pas un mur
            {
                if (m.isSquareBox(x)) //s'il y a une bo�te
                    mat_mario[l][c]= 0;
                else mat_mario[l][c]=1; //sinon mario peut y aller
            }

        file.push(x + mouvement);

    } // file vide

    return mat_mario;

}



/*int * Noeud::BFS(Noeud n, Maze m, Graphic g) /// methode de l'algorithme BFS
{
    ///d�claration
    int* marques; //tableau de marquage
    char* chemin; //chemin le plus court
    int x,y; //sommets interm�diaire
    int i=0;

    ///Initialisation

    n.m_pos_actuel_player = m.getPosPlayer();

    for (unsigned short i=0; i<m_pos_boxes.size(); i++)
        {
            n.pos_actuel_boxes[i]= m_pos_boxes[i];
        }

    marques= new int[ordre]; //allocation dynamique

    for (x=0; x<ordre; x++)
    {
        pred[x]=0;
        marques[x]=0;
    }

    ///D�but de l'algo

    while (m._isCompleted==false) //tant que le niveau n'est pas compl�t�
    {

        for (int i=0; i<m_pos_boxes.size(); i++)
        {

        marques[m_pos_actuel_boxes[i]]=1;
        std::queue<char> file;
        file.pushback(m_pos_actuel_boxes[i]);

            while (!file.empty()) ///tant que la file n'est pas vide
            {
            file.pop(m_pos_actuel_boxes[i]);

                if ( marques[m_pos_actuel_boxes[i]+TOP]== 0 && isSquareWalkable(m_pos_actuel_boxes[i]+TOP)==true)
                {
                marques[m_pos_actuel_boxes[i]+TOP]=1;
                file.push(m_pos_actuel_boxes[i]+TOP);
                pred[m_pos_actuel_boxes[i]+TOP]= m_pos_actuel_boxes[i];
                }

                if ( marques[m_pos_actuel_boxes[i]+BOTTOM]== 0 && isSquareWalkable(m_pos_actuel_boxes[i]+BOTTOM)==true)
                {
                marques[m_pos_actuel_boxes[i]+BOTTOM]=1;
                file.push(m_pos_actuel_boxes[i]+BOTTOM);
                pred[m_pos_actuel_boxes[i]+BOTTOM]= m_pos_actuel_boxes[i];
                }

                if ( marques[m_pos_actuel_boxes[i]+LEFT]== 0 && isSquareWalkable(m_pos_actuel_boxes[i]+LEFT)==true)
                {
                marques[m_pos_actuel_boxes[i]+LEFT]=1;
                file.push(m_pos_actuel_boxes[i]+LEFT);
                pred[m_pos_actuel_boxes[i]+LEFT]= m_pos_actuel_boxes[i];
                }

                if ( marques[pm_pos_actuel_boxes[i]+RIGHT]== 0 && isSquareWalkable(m_pos_actuel_boxes[i]+RIGHT)==true)
                {
                marques[m_pos_actuel_boxes[i]+RIGHT]=1;
                file.push(m_pos_actuel_boxes[i]+RIGHT);
                pred[m_pos_actuel_boxes[i]+RIGHT]= m_pos_actuel_boxes[i];
                }

            }

        chemin[0]= m_pos_actuel_boxes[i];
        while (chemin[i] != m_pos_boxes[i])
        {
        chemin[i+1]=pred[i].
        i++;
        }
        }
    }

    return chemin;
}*/




