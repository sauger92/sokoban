/************************************************************
Sokoban project - Main file
Copyright Florent DIEDLER
Date : 27/02/2016

Please do not remove this header, if you use this file !
************************************************************/
#include "maze.h"
#include "graphic.h"
#include "noeud.h"

#include "utils/console.h"
#include "utils/coord.h"
#include <iostream>
#include "time.h"
#include "stdlib.h"
#include "stdio.h"
#include <string>

#include <chrono>
#include <thread>


// Init allegro with 800x600 (resolution)
Graphic g(800,600,32);

int main()
{
    // Do not touch !
    Console::getInstance()->setColor(_COLOR_DEFAULT);
    if(g.init())
    {

    MenuDebut : ;
    std::string chaine =g.menu(g);
    rest(1000);
    // Load level from a file

    Maze m(chaine);
    if (!m.init()) return -1;

    int choix =0;


    Noeud n;
    std::cout << m << std::endl;



        // While playing...
        while (!g.keyGet(KEY_ESC))
        {
            if (choix!=5)choix=0;
            if (choix == 0)
            choix = g.Menu2(g);///l'utilisateur choisit l'algorithme qu'il souhaite utiliser


            if (choix == 1)
            {
                long double tempDexecution = clock();//Permet d'obtnir le temps entre le lancement du jeu et le lancement de l'algo
                m.detectionDeadLock();
                m.bruteForce(m, g);
                std::cout << "temps d'execution : " << (clock()-tempDexecution)/1000 << std::endl;
            }
            if (choix==2)
            {

                m.detectionDeadLock();
                long double tempDexecution = clock();
                m.Bfs(m, g);
                std::cout << "temps d'execution : " << (clock()-tempDexecution)/1000 << std::endl;


            }
            if (choix==3)
            {

                m.detectionDeadLock();
                long double tempDexecution = clock();
                m.Bfs(m, g);
                std::cout << "temps d'execution : " << (clock()-tempDexecution)/1000 << std::endl;

            }
            if (choix==4)
            {
                m.detectionDeadLock();
                long double tempDexecution = clock();
                m.BestFirstChoice(m, g);
                std::cout << "temps d'execution : " << (clock()-tempDexecution)/1000 << std::endl;
            }
            if (choix==6)
            {
                goto MenuDebut;
            }



            if (g.keyPressed())
            {

                bool win = false;
                switch (g.keyRead())
                {
                    case ARROW_UP:
                        win = m.updatePlayer(TOP);
                        break;
                    case ARROW_BOTTOM:
                        win = m.updatePlayer(BOTTOM);
                        break;
                    case ARROW_RIGHT:
                        win = m.updatePlayer(RIGHT);
                        break;
                    case ARROW_LEFT:
                        win = m.updatePlayer(LEFT);
                        break;
                }

                if (win) std::cout << "Win ! " << std::endl;
            }

            // Display on screen
            g.clear();
            m.draw(g);
            g.display(Coord::m_nb_col);

        }
    }


    // Free memory
    g.deinit();
    Console::deleteInstance();

    return 0;
}
END_OF_MAIN()
